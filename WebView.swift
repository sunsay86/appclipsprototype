//
//  WebView.swift
//  RochevStudio
//
//  Created by Александр Волков on 22.02.2021.
//


import Foundation
import WebKit
import SwiftUI

struct WebView: UIViewRepresentable  {
  var url: String
  
  func makeUIView(context: Context) -> WKWebView {
    guard let url = URL(string: self.url) else {
      return WKWebView()
    }
    let request = URLRequest(url: url)
    let wkWebView = WKWebView()
    wkWebView.load(request)
    return wkWebView
  }
  
  func updateUIView(_ uiView: WebView.UIViewType, context: UIViewRepresentableContext<WebView>) {
    
  }
}

