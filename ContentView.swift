//
//  ContentView.swift
//  RochevStudio
//
//  Created by Александр Волков on 22.02.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        WebView(url: "https://www.rochev.studio")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
