//
//  RochevStudioApp.swift
//  RochevStudio
//
//  Created by Александр Волков on 22.02.2021.
//

import SwiftUI

@main
struct RochevStudioApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
